#!/bin/perl -w
# Min Xu
# ESSIC
# 2011/06/05
#
# Purpose: 1) To download data based on the user's setting in the namelist (43B1, 15A2 and 12Q1)
#          2) To preprocess to the binary files for 43B1 data
#          3) To remap to the grid resolution set by users (only support LCC)
#          4) To clip the domain set by users from the large domain
#          5) To optimize the albedo using LAI and soil moisture if the data are availabe (currently only supoort NARR)
# 2012/09/08 : Min Xu: V1.1 : (1) add ocean tiles 
#                             (2) implement sub reference for the mask function
# 2012/12/10 : Min Xu: V1.2 : (1) add complex command line 
#                             (2) implement multithreading program to process data in parallel mode

      use strict;
      use Getopt::Long;
      use Switch;
      use File::Path;
      use threads;
      use threads::shared;
      use safecwd;
      use Cwd; 

      # global variables
      my $PREFIX = '../';
      my $CASENM = 'CWRFSBC';

      our ($NsdsGBL, $MaskGBL, $MprgGBL, %RmdsGBL);
      my  %LogsGBL;

      # namelist items used in perl
      #
      my ($MaskALB, $MaskLAI, $MaskVEG, $MaskVCF, $MaskMVI, $MaskHVI, $MaskBGC);
      my ($NsdsALB, $NsdsLAI, $NsdsVEG, $NsdsVCF, $NsdsMVI, $NsdsHVI, $NsdsBGC);

      # 
      my ($DoneALB, $DoneVEG, $DoneLAI, $DoneMVI, $DoneHVI, $DoneBGC, $DoneVCF, $DoneSOM, $DmonSOM, $D3hrSOM, $DoneFVC, $DoneAVH); 
      my ($ProcALB, $ProcLAI, $ProcVEG, $ProcMVI, $ProcHVI, $ProcBGC, $ProcVCF, $ProcSOM);
      my ($FlogALB, $FlogLAI, $FlogVEG, $FlogMVI, $FlogHVI, $FlogBGC, $FlogSOM);

      my ($RmapPRG, $TilePRG, $MaskPRG, $ConvPRG, $UnpkPRG, $AgrgPRG);
      my (%list_jv);

      my ($FtpGDAS, $Ftp4MDS);

      # Date Information
      my ($yrsDATA, $jdsDATA, $yreDATA, $jdeDATA, $jdv4MDS);
      my ($yrsPARA, $jdsPARA, $yrePARA, $jdePARA);
      my ($yrs_VEG, $jds_VEG, $yre_VEG, $jde_VEG);

      my ($domNAME, $domXDIM, $domYDIM, $fvcQUAL, $mapTYPE, $sizXDIM, $sizYDIM, $GRIDSIZ, $GLOBDIR, $GEOGRID);

      # options:
      my ($ParaALB, $FillALB, $FillVEG, $FillLAI); 


      my ($iy, $im, $jd, $ij);
      my ($cy, $cm, $cd, $vr);
      my ($jdstrt, $jdstop);
      my ($iystrt, $iystop);

      my (@fnHDF, $fnLST, $fnFIL);

      my ($HelpPrt, $Version, $Muthred);
      my ($NumPths, $NumDths, @pth, @qth);

      $ProcALB = 0;
      $ProcVEG = 0;
      $ProcLAI = 0;
      $ProcMVI = 0;
      $ProcHVI = 0;
      $ProcVCF = 0;
      $ProcSOM = 0;

      #default

      $DoneAVH = 1;
      $DoneFVC = 1;

      $Version = "\n\nVersion 1.5 2011-2013, Author: Min Xu (minxu AT umd.edu), ESSIC, UMD\n\n"; 
      &help_messages() if($HelpPrt || $#ARGV < 0);
      my $rc = GetOptions(
                 "ALB|a:2"    => \$ProcALB, 
                 "LAI|l:1"    => \$ProcLAI,
                 "VEG|u:1"    => \$ProcVEG,
                 "VCF|c:1"    => \$ProcVCF,
                 "MVI|i:1"    => \$ProcMVI,
                 "HVI|j:1"    => \$ProcHVI,
                 "BGC|b:1"    => \$ProcBGC,
                 "SOM|s:1"    => \$ProcSOM,
                 "MTh|m"      => \$Muthred,
                 "help|h"     => \$HelpPrt,
                 "IVT!"       => \$DoneAVH,
                 "FVC!"       => \$DoneFVC,
                 "prefix|p:s" => \$PREFIX,
                 "version|v"  => sub{print $Version if(shift); exit 0;});                 


      &help_messages() if($HelpPrt);

      $DoneALB = 1; $DoneALB = 0 if($ProcALB >= 1);
      $DoneVEG = 1; $DoneVEG = 0 if($ProcVEG >= 1);
      $DoneVCF = 1; $DoneVCF = 0 if($ProcVCF >= 1);
      $DoneLAI = 1; $DoneLAI = 0 if($ProcLAI >= 1);
      $DoneMVI = 1; $DoneMVI = 0 if($ProcMVI >= 1);
      $DoneHVI = 1; $DoneHVI = 0 if($ProcHVI >= 1);
      $DoneBGC = 1; $DoneBGC = 0 if($ProcBGC >= 1);
      $DoneSOM = 1; $DoneSOM = 0 if($ProcSOM >= 1);     
      $NumPths = 6;

      # no parallel on VEG
      $ProcVEG = 1 if($ProcVEG >= 1);

      # threadings
      if($Muthred)
      {
         $NumDths =            $ProcALB + $ProcVEG + $ProcVCF + $ProcLAI + $ProcMVI + $ProcHVI + $ProcBGC + $ProcSOM;
         $NumPths = $NumPths - $DoneALB - $DoneVEG - $DoneVCF - $DoneLAI - $DoneMVI - $DoneHVI - $DoneBGC - $DoneSOM;
      }
      else
      {
         $NumDths = 1;
         $NumPths = 1;
      }

      print "$ProcVEG, $ProcLAI, $NumDths, $NumPths\n";
      
      #initilize to false
      my $iCWPSH = cwd();

      my (%is_ocn, @ti_ocn);
      @ti_ocn = qw(
         h00v07 h01v06 h01v07 h02v05 h02v07 h03v05 h03v08 h03v09 h03v12 h04v04 h04v05 h04v06 h04v07 h04v08 h04v12 h04v13
         h05v04 h05v05 h05v06 h05v07 h05v08 h05v09 h05v12 h06v04 h06v05 h06v06 h06v07 h06v08 h06v09 h06v10 h06v12 h06v13 h06v14
         h24v08 h24v09 h24v10 h24v11 h24v13 h24v14 h25v10 h25v11 h25v12 h25v13 h25v14 h25v15 h26v09 h26v10 h26v11 h26v12 h26v13 h26v14 h26v15 
         h29v04 h30v04 h31v04 h31v05 h32v05 h32v06 h33v05 h33v06 h33v12 h34v06 h34v11 h35v07 h07v04 h13v05 h13v06 h13v07 h12v06 h14v05);

      undef %is_ocn; %is_ocn = map{$_ => 1} @ti_ocn;

  
      $FtpGDAS = 'ftp://hydro1.sci.gsfc.nasa.gov/data/s4pa/GLDAS/GLDAS_NOAH10_M_E1.002/';

      $DmonSOM = 1;
      $D3hrSOM = 0;  #DoneSOM should be zero

      $FillALB = 1;
      $FillVEG = 1;
      $FillLAI = 1;

      &CompileFsrc;

      # +xum define some hash structures
      my %DirHash = (
         'alb' => ["$PREFIX/MODIS/ALB/", "$PREFIX/iCWPS_1km/ALB/", "$PREFIX/iCWPS_nkm/ALB/", "$PREFIX/wk/ALB/"],
         'lai' => ["$PREFIX/MODIS/LAI/", "$PREFIX/iCWPS_1km/LAI/", "$PREFIX/iCWPS_nkm/LAI/", "$PREFIX/wk/LAI/"],
         'veg' => ["$PREFIX/MODIS/VEG/", "$PREFIX/iCWPS_1km/VEG/", "$PREFIX/iCWPS_nkm/VEG/", "$PREFIX/wk/VEG/"],
         'vcf' => ["$PREFIX/MODIS/VCF/", "$PREFIX/iCWPS_1km/VCF/", "$PREFIX/iCWPS_nkm/VCF/", "$PREFIX/wk/VCF/"],
         'mvi' => ["$PREFIX/MODIS/MVI/", "$PREFIX/iCWPS_1km/MVI/", "$PREFIX/iCWPS_nkm/MVI/", "$PREFIX/wk/MVI/"],
         'hvi' => ["$PREFIX/MODIS/HVI/", "$PREFIX/iCWPS_1km/HVI/", "$PREFIX/iCWPS_nkm/HVI/", "$PREFIX/wk/HVI/"],
         'fvc' => ["$PREFIX/MODIS/FVC/", "$PREFIX/iCWPS_1km/FVC/", "$PREFIX/iCWPS_nkm/FVC/", "$PREFIX/wk/FVC/"],
         'som' => ["$PREFIX/MODIS/SOM/", "$PREFIX/iCWPS_1km/SOM/", "$PREFIX/iCWPS_nkm/SOM/", "$PREFIX/wk/SOM/"],
         'opt' => ["$PREFIX/MODIS/OPT/", "$PREFIX/iCWPS_1km/OPT/", "$PREFIX/iCWPS_nkm/OPT/", "$PREFIX/wk/OPT/"],
         'bgc' => ["$PREFIX/MODIS/BGC/", "$PREFIX/iCWPS_30s/BGC/", "$PREFIX/iCWPS_arc/BGC/", "$PREFIX/wk/BGC/"],
      );

      my %LogHash = (
         'alb' => ["${$DirHash{'alb'}}[3]/Log.alb", $ProcALB, $FlogALB], 
         'lai' => ["${$DirHash{'lai'}}[3]/Log.lai", $ProcLAI, $FlogLAI], 
         'veg' => ["${$DirHash{'veg'}}[3]/Log.veg", $ProcVEG, $FlogVEG], 
         'vcf' => ["${$DirHash{'vcf'}}[3]/Log.vcf", $ProcVEG, $FlogVEG], 
         'mvi' => ["${$DirHash{'mvi'}}[3]/Log.mvi", $ProcMVI, $FlogMVI], 
         'hvi' => ["${$DirHash{'hvi'}}[3]/Log.hvi", $ProcHVI, $FlogHVI], 
         'bgc' => ["${$DirHash{'bgc'}}[3]/Log.bgc", $ProcBGC, $FlogBGC], 
      );

      my $pd = 0;
      my %MdsProd = (
         "MCD43B1.A" => ["alb", $pd],
         "MCD15A2.A" => ["lai", $pd],
         "MCD12Q1.A" => ["veg", $pd],
         "MOD44B.A"  => ["vcf", $pd],
         "MOD13A2.A" => ["mvi", $pd],
         "MOD13Q1.A" => ["hvi", $pd],
         "MOD17A3.A" => ["bgc", $pd],
      );


      # parse namelist to get the variables that we want 
      $RmapPRG = &ParseNML('map_prg_nam'); 
      $TilePRG = &ParseNML('til_prg_nam'); 
      $MaskPRG = &ParseNML('msk_prg_nam');
      $UnpkPRG = &ParseNML('upk_prg_nam');
      $ConvPRG = &ParseNML('cnv_prg_nam');
      $AgrgPRG = &ParseNML('agg_prg_nam');

      $MaskALB = &ParseNML('msk_set_alb');
      $MaskLAI = &ParseNML('msk_set_lai');
      $MaskVEG = &ParseNML('msk_set_veg');
      $MaskVCF = &ParseNML('msk_set_vcf');
      $MaskMVI = &ParseNML('msk_set_mvi');
      $MaskHVI = &ParseNML('msk_set_hvi');
      $MaskBGC = &ParseNML('msk_set_bgc');

      $NsdsALB = &ParseNML('sds_set_alb');
      $NsdsLAI = &ParseNML('sds_set_lai');
      $NsdsVEG = &ParseNML('sds_set_veg');
      $NsdsVCF = &ParseNML('sds_set_vcf');
      $NsdsMVI = &ParseNML('sds_set_mvi');
      $NsdsHVI = &ParseNML('sds_set_hvi');
      $NsdsBGC = &ParseNML('sds_set_bgc');
      # 
      $Ftp4MDS = &ParseNML('ftp_mds_url');
      #
      $yrsDATA = &ParseNML('iyr_dat_bgn');
      $yreDATA = &ParseNML('iyr_dat_end');
      $jdsDATA = &ParseNML('jdy_dat_bgn');
      $jdeDATA = &ParseNML('jdy_dat_end');

      $jdv4MDS = &ParseNML('idy_dat_inv');
      
      $yrsPARA = &ParseNML('iyr_par_bgn');
      $yrePARA = &ParseNML('iyr_par_end');
      $jdsPARA = &ParseNML('jdy_par_bgn');
      $jdePARA = &ParseNML('jdy_par_end');


      $jds_VEG = &ParseNML('veg_cov_bgn');
      $jde_VEG = &ParseNML('veg_cov_end');

      $jds_VEG =~/^(\d{4})(\d{3})$/; $yrs_VEG = $1; $jds_VEG = $2;
      $jde_VEG =~/^(\d{4})(\d{3})$/; $yre_VEG = $1; $jde_VEG = $2;

  
      $domNAME = &ParseNML('dta_dom_nam');

      #xum, in future, this part should be read from namelist.wps
      $mapTYPE = &ParseNML('map_prj_typ');
      $fvcQUAL = &ParseNML('fvc_qal_ctl');
      $sizXDIM = &ParseNML('dta_grd_xnm');
      $sizYDIM = &ParseNML('dta_grd_ynm');

      $GRIDSIZ = &ParseNML('flt_grd_siz');

      $GLOBDIR = &ParseNML('glob_static');
      $GEOGRID = &ParseNML('geogrid_nml');

      $domXDIM = int($sizXDIM / $GRIDSIZ);
      $domYDIM = int($sizYDIM / $GRIDSIZ);

      die "\n\nErr: Please correctly set the map_prg_nam in namelist, $RmapPRG do not exist \n" if(! -e $RmapPRG);
      die "\n\nErr: Please correctly set the map_prg_nam in namelist, $TilePRG do not exist \n" if(! -e $TilePRG);
      die "\n\nErr: Please correctly set the map_prg_nam in namelist, $MaskPRG do not exist \n" if(! -e $MaskPRG);
      die "\n\nErr: Please correctly set the map_prg_nam in namelist, $UnpkPRG do not exist \n" if(! -e $UnpkPRG);
      die "\n\nErr: Please correctly set the map_prg_nam in namelist, $ConvPRG do not exist \n" if(! -e $ConvPRG);

      die "\n\nErr: Please correctly set the day interval for MODIS \n" if($jdv4MDS != 8 && $jdv4MDS != 16);

      foreach my $key (keys %DirHash)
      {
          &PrepareDirs(@{$DirHash{$key}}   );
          &SymLnkFiles(${$DirHash{$key}}[3]);
      }

      #generate 1-km data from Global 30' static data

      print $DoneAVH, $DoneFVC;
      if($DoneAVH)
      {
         chdir $DirHash{'veg'}->[3];
         system("./RDGBLDAT $GLOBDIR lcc2") == 0 || die "in RDGBLDAT lcc2 $! \n";
         symlink "lcc2.dat", "avhrr_usgs.bin";
         system("./RDGBLDAT $GLOBDIR lcc3") == 0 || die "in RDGBLDAT lcc3 $! \n";
         symlink "lcc3.dat", "avhrr_igbp.bin";
      }

      if($DoneFVC)
      {
         chdir $DirHash{'fvc'}->[3];
         system("./RDGBLDAT $GLOBDIR VCF" ) == 0 || die "in RDGBLDAT VCF  $! \n";
         system("/bin/mv -f VCF.dat $DirHash{'fvc'}->[1]/MOD44BW.Aclimate.fvc.$fvcQUAL.$domNAME.$mapTYPE.1km.${sizXDIM}x${sizYDIM}.rcm") == 0 || die "error in link \n"; 
         chdir $iCWPSH;
      }


      $ij = $jdv4MDS; 

      %list_jv = &DomainSet(${$DirHash{'lai'}}[3]);

      print "aaaaaaaaaaaaaaaaaaaaaaa\n";
      # MODIS Main Loop 
      $iystrt = $yrsDATA;
      $iystop = $yreDATA;
      foreach $iy (($iystrt .. $iystop))
      {
          $cy = sprintf("%04d", $iy);

          if($iy == $iystrt) {$jdstrt = $jdsDATA;}
          else               {$jdstrt = 1       ;}

          if($iy == $iystop) {$jdstop = $jdeDATA;}
          else               {$jdstop = 366     ;}

          if(! $DoneVEG && $iy >= $yrs_VEG && $iy <= $yre_VEG) 
          {
              # processing the vegetation data
              $cd = '001';
              $vr = '005';  # Min Xu, 2013/08/10, due to unavailable data LPDACC pool, change to 051
              $vr = '051';

              # -----------------------------------------------------------------------------------------------
              if($Muthred)
              { 
                   my $jds = 1;
                   my $jde = 1;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'veg'}};
                   my @subarg = (\@times, \@dirnm, 'MCD12Q1.A', \%list_jv, $vr, \&mask_data_veg, $NsdsVEG, $MaskVEG);
                   my $thr = threads->create('proc_mds_sub', @subarg); 
                   push @pth, $thr;

              }
              else         
              { 
                   my $jds = 1;
                   my $jde = 1;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'veg'}};
                   my @subarg = (\@times, \@dirnm, 'MCD12Q1.A', \%list_jv, $vr, \&mask_data_veg, $NsdsVEG, $MaskVEG);
                   &proc_mds_sub(@subarg) ;
              }

              #&Clean_Files(\@fnHDF);
              #exit;
          }


          if(! $DoneBGC && $iy >= $yrs_VEG && $iy <= $yre_VEG)
          {
              # processing the vegetation data
              $cd = '001';
              $vr = '005';  # Min Xu, 2013/08/10, due to unavailable data LPDACC pool, change to 051
              $vr = '055';

              # -----------------------------------------------------------------------------------------------
              if($Muthred)
              {
                   my $jds = 1;
                   my $jde = 1;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'bgc'}};
                   my @subarg = (\@times, \@dirnm, 'MOD17A3.A', \%list_jv, $vr, \&mask_data_bgc, $NsdsBGC, $MaskBGC);
                   my $thr = threads->create('proc_mds_sub', @subarg);
                   push @pth, $thr;

              }
              else
              {
                   my $jds = 1;
                   my $jde = 1;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'bgc'}};
                   my @subarg = (\@times, \@dirnm, 'MOD17A3.A', \%list_jv, $vr, \&mask_data_bgc, $NsdsBGC, $MaskBGC);
                   &proc_mds_sub(@subarg) ;
              }

              #&Clean_Files(\@fnHDF);
              #exit;
          }


          if(! $DoneVCF && $iy >= $yrs_VEG && $iy <= $yre_VEG)
          {
              # processing the vegetation data
              $cd = '065';
              $vr = '005'; 

              # -----------------------------------------------------------------------------------------------
              if($Muthred)
              {
                   my $jds = 65;
                   my $jde = 65;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'vcf'}};
                   my @subarg = (\@times, \@dirnm, 'MOD44B.A', \%list_jv, $vr, \&mask_data_vcf, $NsdsVCF, $MaskVCF);
                   my $thr = threads->create('proc_mds_sub', @subarg);
                   push @pth, $thr;

              }
              else
              {
                   my $jds = 65;
                   my $jde = 65;
                   my $jdv = $ij;

                   my @times = ($cy,$jds,$jde,$jdv);
                   my @dirnm = @{$DirHash{'vcf'}};
                   my @subarg = (\@times, \@dirnm, 'MOD44B.A', \%list_jv, $vr, \&mask_data_vcf, $NsdsVCF, $MaskVCF);
                   &proc_mds_sub(@subarg) ;
              }

              #&Clean_Files(\@fnHDF);
              #exit;
          }


          if(! $DoneLAI)
          {
              $vr = '005';
              if($Muthred && $ProcLAI >= 1)
              {
                   for(my $pd=0; $pd< $ProcLAI; $pd++)
                   { 
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcLAI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'lai'}};
                       my @subarg = (\@times, \@dirnm, 'MCD15A2.A', \%list_jv, $vr, \&mask_data_lai, $NsdsLAI, $MaskLAI);
                       my $thr = threads->create('proc_mds_sub', @subarg);
                       push @pth, $thr;
                   }
              }
              else
              {
                   for(my $pd=0; $pd < 1; $pd++)
                   {
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcLAI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'lai'}};
                       my @subarg = (\@times, \@dirnm, 'MCD15A2.A', \%list_jv, $vr, \&mask_data_lai, $NsdsLAI, $MaskLAI);
                       &proc_mds_sub(@subarg) ;
                   }
              }
          }

          if(! $DoneMVI)
          {
              $vr = '005';
              if($Muthred && $ProcMVI >= 1)
              {
                   for(my $pd=0; $pd< $ProcMVI; $pd++)
                   { 
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcMVI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'mvi'}};
                       my @subarg = (\@times, \@dirnm, 'MOD13A2.A', \%list_jv, $vr, \&mask_data_mvi, $NsdsMVI, $MaskMVI);
                       my $thr = threads->create('proc_mds_sub', @subarg);
                       push @pth, $thr;
                   }

              }
              else
              {
                   for(my $pd=0; $pd < 1; $pd++)
                   { 
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcMVI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'mvi'}};
                       my @subarg = (\@times, \@dirnm, 'MOD13A2.A', \%list_jv, $vr, \&mask_data_mvi, $NsdsMVI, $MaskMVI);
                       &proc_mds_sub(@subarg) ;
                   }
              }
          }

          if(! $DoneHVI)
          {
              $vr = '005';
              if($Muthred && $ProcHVI >= 1)
              {
                   for(my $pd=0; $pd< $ProcHVI; $pd++)
                   {
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcHVI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'hvi'}};
                       my @subarg = (\@times, \@dirnm, 'MOD13Q1.A', \%list_jv, $vr, \&mask_data_hvi, $NsdsHVI, $MaskHVI);
                       my $thr = threads->create('proc_mds_sub', @subarg);
                       push @pth, $thr;
                   }

              }
              else
              {
                   for(my $pd=0; $pd < 1; $pd++)
                   {
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcHVI;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'hvi'}};
                       my @subarg = (\@times, \@dirnm, 'MOD13Q1.A', \%list_jv, $vr, \&mask_data_hvi, $NsdsHVI, $MaskHVI);
                       &proc_mds_sub(@subarg) ;
                   }
              }
          }


          if(! $DoneALB)
          {
              $vr = '005';
              if($Muthred && $ProcALB >= 1)
              {
                   for(my $pd=0; $pd< $ProcALB; $pd++)
                   { 
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcALB;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'alb'}};
                       my @subarg = (\@times, \@dirnm, 'MCD43B1.A', \%list_jv, $vr, \&mask_data_alb, $NsdsALB, $MaskALB);
                       my $thr = threads->new('proc_mds_sub', @subarg);
                       push @pth, $thr;
                   }

              }

              else
              {
                   for(my $pd=0; $pd < 1; $pd++)
                   {
                       my $jds = $jdstrt + $pd*$ij;
                       my $jde = $jdstop;
                       my $jdv = $ij * $ProcALB;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'alb'}};
                       my @subarg = (\@times, \@dirnm, 'MCD43B1.A', \%list_jv, $vr, \&mask_data_alb, $NsdsALB, $MaskALB);
                       &proc_mds_sub(@subarg);
                   }
              }
          }

          # Download the monthly data from GLDAS-NOAH (exp)
          #
          if(! $DoneSOM)
          {
              if($Muthred && $ProcSOM >= 1)
              {
                   for(my $pd=0; $pd< $ProcSOM; $pd++)
                   {
                       my $jds = $jdstrt + $pd*1;
                       my $jde = 365 + isleap($iy);
                       #my $jde = 213;
                       print "xxx $jde \n";
                       my $jdv = 1 * $ProcSOM;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'som'}};
                       my @subarg = (\@times, \@dirnm, 1, 0);
                       my $thr = threads->create('proc_gds_sub', @subarg);
                       push @pth, $thr;
                   }
              }
              else
              {
                   for(my $pd=0; $pd < 1; $pd++)
                   {
                       my $jds = 1   + $pd*1;
                       my $jde = 365 + isleap($iy);
                       my $jdv = 1 * $ProcSOM;

                       my @times = ($cy,$jds,$jde,$jdv);
                       my @dirnm = @{$DirHash{'som'}};
                       my @subarg = (\@times, \@dirnm, 1, 0);
                       &proc_gds_sub(@subarg) ;
                   }
              }
          }


          # now processing the 1km data
          my $bgn = $cy.sprintf("%03d", $jdsDATA);
          my $end = $cy.sprintf("%03d", $jdeDATA);

          my $stpthrlai = 0;
          my $stpthrveg = 0;
          my $stpthralb = 0;
          my @nth;

          if($Muthred)
          {

 
             @nth = @pth;
             while($#nth >= 0)
             {
                  foreach my $thr (@pth)
                  {
                     if($thr->is_joinable())
                     {
                          my $val = $thr->join();
                          print "xxx", $thr->is_running(), $val, "\n";
                          switch($val)
                          {
                              case('alb')
                              {
                                  $stpthralb++;
                              }
                              case('lai')
                              {
                                  $stpthrlai++;
                              }
                              case('veg')
                              {
                                  $stpthrveg++;
                              }
                              case('vcf') {}
                              case('mvi') {}
                              case('hvi') {}
                              case('bgc') {}
                              case('gds') {}
                              case('som') {}
                              else        {die "\n Err in downloading and masking processes \n";}
                          }

                          @nth = grep{$_ != $thr} @nth;
                     }
                  }
                  print "threading ...@nth, $#nth \n";
                  sleep 10;
             }
             @pth = @nth;
          }
          else
          {
             $stpthrlai = $ProcLAI;
             $stpthrveg = $ProcVEG;
             $stpthralb = $ProcALB;
          }



          #print 'xxx test threads, LAI', " $Muthred, $stpthrlai ,$ProcLAI,  $stpthrveg, $ProcVEG\n";
          if($stpthrlai == $ProcLAI && $stpthrveg == $ProcVEG && $ProcLAI > 0)
          {
             if($Muthred)
             {
                my @subarg = ("MCD15A2.A", "hq", $bgn, $end, $jdv4MDS, 1, 2);
                my $th2 = threads->new({'void' => 1}, sub{my ($mdsnm, $mdsqa, $bgnjd, $endjd, $invjd, $bgnrc, $endrc)=@_;
                system("cd $DirHash{'lai'}->[3] && $AgrgPRG $mdsnm $mdsqa $bgnjd $endjd $invjd $bgnrc $endrc > log.$cy") == 0 || die "error im agg for lai\n";}, @subarg);
                push @qth, $th2;
             }
             else
             {
                system("cd $DirHash{'lai'}->[3] && $AgrgPRG 'MCD15A2.A' 'hq' $bgn $end $jdv4MDS 1 2") == 0 || die "error im agg for lai\n";
             }
          }

          #print 'xxx test threads, ALB', " $Muthred, $stpthralb , $ProcALB, $stpthrveg, $ProcVEG\n";
          if($stpthralb == $ProcALB && $stpthrveg == $ProcVEG && $ProcALB > 0)
          {
             if($Muthred)
             {
                my @subarg = ("MCD43B1.A", "hq", $bgn, $end, $jdv4MDS, 1, 9);
                my $th2 = threads->new({'void' => 1}, sub{my ($mdsnm, $mdsqa, $bgnjd, $endjd, $invjd, $bgnrc, $endrc)=@_;
                system("cd $DirHash{'alb'}->[3] && $AgrgPRG $mdsnm $mdsqa $bgnjd $endjd $invjd $bgnrc $endrc > log.$cy") == 0 || die "error im agg for alb\n";}, @subarg);
                push @qth, $th2;
             }
             else
             {
                system("cd $DirHash{'alb'}->[3] && $AgrgPRG 'MCD43B1.A' 'hq' $bgn $end $jdv4MDS 1 9") == 0 || die "error im agg for alb\n";
             }
          }
          if($Muthred)
          {
             foreach my $th2 (@qth)
             {
                 $th2->join();
             }

             # delete qth every year, otherwise, it will return error for the last year already joined thread
             @qth=();
          }

          print (keys %RmdsGBL);

      } # year loop
      # above is the year loop

      $DoneVEG = 1 if($ProcVEG >= 1 && &check_mds_data(${$DirHash{'veg'}}[1], $yrs_VEG, $yre_VEG, 0));
      $DoneLAI = 1 if($ProcLAI >= 1 && &check_mds_data(${$DirHash{'lai'}}[1], $yrsPARA, $yrePARA, 8));
      $DoneALB = 1 if($ProcALB >= 1 && &check_mds_data(${$DirHash{'alb'}}[1], $yrsPARA, $yrePARA, 8));
      $DoneSOM = 1;
      
      # remap LCC
      if($DoneVEG && $ProcVEG > 0 && $DoneAVH)
      {
           chdir $DirHash{'veg'}->[3];
           system("./GENCMVEG $DirHash{'veg'}->[1] 'hq' $domNAME $mapTYPE $yrs_VEG $yre_VEG $domXDIM $domYDIM $GRIDSIZ") == 0 || die "Error in GEN VEG CLIM \n";
           system("./REMAPLCC $domXDIM $domYDIM $GRIDSIZ") == 0 || die "Error in Remap LCC \n";
           system("cd $DirHash{'opt'}->[2] && ln -sf $DirHash{'veg'}->[3]/domvtype_usgs24.dat domvtype.dat") == 0 || die "error in link vtype";
           chdir $iCWPSH;
      }

      if($DoneLAI && $ProcLAI > 0)
      {

           my $fmx = int(log($domXDIM)/log(10.))+1;
           my $fmy = int(log($domYDIM)/log(10.))+1;
           my $fvcname = "MOD44BW.Aclimate.dom.fvc.hq.$domNAME.$mapTYPE.".sprintf($fmx,$domXDIM)."x".sprintf($fmy,$domYDIM).".cwrf";


           chdir $DirHash{'lai'}->[3];

           system("./MODISAGG MOD44BW.A hq 2003001 2011366 8 1 1") == 0 || die "Error in AGG FVC\n";
           system("/bin/ln -sf $DirHash{'veg'}->[3]/domvtype_usgs24.dat domvtype.dat") == 0 || die "Error in link domvtype \n";
           system("/bin/ln -sf $DirHash{'fvc'}->[2]/$fvcname fvc.bin") == 0 || die "error in link fvc \n";
           system("./GENCMLAI $DirHash{'lai'}->[1] 'hq' $domNAME $mapTYPE $yrsPARA $yrePARA $domXDIM $domYDIM $GRIDSIZ") == 0 || die "Error in GEN LAI CLIM \n";
           chdir $iCWPSH;
      }


      if($DoneVEG && $DoneLAI && $DoneALB && $DoneSOM && $ParaALB)
      {

           print "now optimizating albedo using MODIS albedo parameters\n";
           print "please be patient, it will take more than 24 hours \n";

           chdir $DirHash{'opt'}->[3];

           system("/bin/ln -sf $DirHash{'lai'}->[2] modis_lai" ) == 0 || die "error in link lai \n";
           system("/bin/ln -sf $DirHash{'alb'}->[2] modis_alb" ) == 0 || die "error in link alb \n";
           system("/bin/ln -sf $DirHash{'som'}->[2] gldas_som" ) == 0 || die "error in link som \n";
           system("/bin/ln -sf $DirHash{'opt'}->[2] cssp_land" ) == 0 || die "error in link som \n";
           system("/bin/ln -sf $DirHash{'fvc'}->[3]/clat.dat ./cssp_land") == 0 || die "error in link lat \n";
           system("/bin/ln -sf $DirHash{'fvc'}->[3]/clon.dat ./cssp_land") == 0 || die "error in link lon \n";

           mkpath("$DirHash{'opt'}->[3]/optresult");
           #minxu need correct this
           system("./LSP_FFSQP nx ny nt 2003001 2010366"       ) == 0 || die "error in optimization albedo \n";

           chdir $iCWPSH;

           print "please check results in $DirHash{'opt'}->[3] \n";
      }

      # end of the main program scope

      # ---------------------------
      # the followings are subs ...
      # ---------------------------

      sub check_mds_data($$$$)
      {
          my ($mdsdir, $strtyr, $stopyr, $jdyinv) = @_;


          my ($chkfile, $iy, $cd, @temp, $allfile);

          
          $allfile = 1;
          if($jdyinv == 0)
          {
              foreach $iy (($strtyr..$stopyr))
              {
                  $chkfile = "A${iy}001";
                  @temp = glob("$mdsdir/*$chkfile*");
                  $allfile = 0 if($#temp < 0);

                  print "in check $temp[0], $allfile, $mdsdir/*$chkfile* \n";
              }

          }
          else
          {
              foreach $iy (($strtyr..$stopyr))
              {
                  for($jd=1; $jd<=366; $jd+=$jdyinv)
                  {
                      $cd = sprintf("%03d", $jd);
                      $chkfile = "A$iy$cd";
                      @temp = glob("*$chkfile*");
                      $allfile = 0 if($#temp < 0);
                  }
              }
          }
          
          return $allfile;
      }

      # compile all fortran codes needed in this scripts
      sub CompileFsrc()
      {
          chdir "./for2";
          system("make -f makefile.resample") == 0 || die "Error in compile resample FORTRAN codes \n";
          system("make -f makefile.remaplcc") == 0 || die "Error in compile remapllc FORTRAN codes \n";
          system("make -f makefile.remaproj") == 0 || die "Error in compile remaproj FORTRAN codes \n";
          system("make -f makefile.rdgbldat") == 0 || die "Error in compile rdgbldat FORTRAN codes \n";
          system("make -f makefile.gencmlai") == 0 || die "Error in compile gencmlai FORTRAN codes \n";
          system("make -f makefile.gencmveg") == 0 || die "Error in compile gencmveg FORTRAN codes \n";
          system("make -f makefile.modisagg") == 0 || die "Error in compile modisagg FORTRAN codes \n";
          chdir "./albopt";
          chdir $iCWPSH;
      }

      # link all the executive file to current working directory $PREFIX 
      sub SymLnkFiles($)
      {
          #chdir $PREFIX || die "\n\ncan't change to the working directory\n\n";
          my $dirwk = shift;
          my $cwd   = cwd();
          chdir $dirwk;
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_alb.ncl .")  ;#if (! -e 'ncl_sds2bin_alb.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_lai.ncl .")  ;#if (! -e 'ncl_sds2bin_lai.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_mvi.ncl .")  ;#if (! -e 'ncl_sds2bin_lai.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_bgc.ncl .")  ;#if (! -e 'ncl_sds2bin_lai.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_veg.ncl .")  ;#if (! -e 'ncl_sds2bin_veg.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_vcf.ncl .")  ;#if (! -e 'ncl_sds2bin_vcf.ncl' ); 
          system("/bin/ln -sf $iCWPSH/ncl/ncl_sds2bin_som.ncl .")  ;#if (! -e 'ncl_sds2bin_som.ncl' ); 

          system("/bin/ln -sf $iCWPSH/for2/REMAPROJ .")            ;#if (! -e 'REMAPROJ'            );
          system("/bin/ln -sf $iCWPSH/for2/RESAMPLE .")            ;#if (! -e 'RESAMPLE'            );
          system("/bin/ln -sf $iCWPSH/for2/REMAPLCC .")            ;#if (! -e 'REMAPLCC'            );
          system("/bin/ln -sf $iCWPSH/for2/RDGBLDAT .")            ;#if (! -e 'RDGBLDAT'            );
          system("/bin/ln -sf $iCWPSH/for2/GENCMLAI .")            ;#if (! -e 'GENCMLAI'            );
          system("/bin/ln -sf $iCWPSH/for2/GENCMVEG .")            ;#if (! -e 'GENCMVEG'            );
          system("/bin/ln -sf $iCWPSH/for2/MODISAGG .")            ;#if (! -e 'MODISAGG'            );

          system("/bin/ln -sf $iCWPSH/for2/albopt/LSP_FFSQP .")    ;

          system("/bin/ln -sf $iCWPSH/for2/namelist.icwps-modis .");# if (! -e 'namelist.icwps-modis');
         #system("/bin/ln -sf $iCWPSH/for2/namelist.geogrid ."    );# if (! -e 'namelist.geogrid'    );
          system("/bin/ln -sf $GEOGRID namelist.wps"              );# if (! -e 'namelist.geogrid'    );
          chdir $cwd;
      }
      # 
      # clean the files
      #
      sub Clean_Files($)
      {
          my $refer;
          my @files;

          $refer = shift;
          @files = @$refer;

          foreach (@files) {unlink;}
      }

      # download alb data and process it and return the full path of the files if not exist

      sub DownMDS_PP($$$$$$$$)
      {
          my ($mds_wdir, $hdf_star, $the_year, $the_jday, $mds_ldir, $mds_name, $mds_snam, $mds_lver, $mds_sens);

          my ($mds_rdir, $mds_durl, $mds_qurl, $the_mnth, $the_cday);

          my ($hdf_data, $hdf_qual, $hdf_qstr, $binfile, @temp, @qual);

          $mds_wdir = shift;
          $hdf_star = shift;     # *.hdf
          $the_year = shift; 
          $the_jday = shift; 
          $mds_ldir = shift;
          $mds_name = shift;     # include ".A"
          $mds_lver = shift;
          $mds_sens = shift;

          #my $cwd = cwd();
          my $cwd = $mds_wdir;
          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};

          ($the_mnth, $the_cday) = &jd2mndy($the_year, $the_jday);

          # check the local directory is exist

          mkdir "$mds_ldir/$the_year" if(! -d "$mds_ldir/$the_year");

          # downloading
          $mds_rdir="";
          switch(lc($mds_sens))
          { 
               case('terra'   ) {$mds_rdir = "MOLT";}
               case('aqua'    ) {$mds_rdir = "MOLA";}
               case('combined') {$mds_rdir = "MOTA";}
               else             {die "\n please give the correct sensor name for MODIS \n";}
          }
          
          $mds_snam = $mds_name;
          $mds_snam =~s/\.A//;    # remove the ".A" 

          $mds_rdir = "$mds_rdir/$mds_snam.$mds_lver/$the_year";

          $mds_rdir.= ".".sprintf("%02d", $the_mnth);
          $mds_rdir.= ".".sprintf("%02d", $the_cday);

          $mds_durl = "$Ftp4MDS/$mds_rdir/$hdf_star";


          # the quality and data are seperated 

          # ALB
          if($mds_name=~/43B1/)
          {
             $mds_qurl = $mds_durl;
             $mds_qurl =~ s/43B1/43B2/g;

             $hdf_qual = wget_modis($mds_qurl, $cwd);
             $hdf_data = wget_modis($mds_durl, $cwd);

             # we need to check if the files is in the directory

             #$hdf_qstr = $hdf_star;
             #$hdf_qstr =~s/43B1/43B2/;
             #@temp = glob("$hdf_star");
             #@qual = glob("$hdf_qstr");

             #if($#temp >=0 && $#qual >=0)  
             #{
             #   $hdf_data = $temp[0];
             #   $hdf_qual = $qual[0];
             if( defined $hdf_qual && defined $hdf_data)
             {

                #$binfile = &mask_data_alb($hdf_data, $NsdsALB, $MaskALB);
                $binfile = $MprgGBL->($cwd, $hdf_data, $hdf_qual, $NsdsGBL, $MaskGBL);

                # compress and move the directory

                system("cd $cwd && /bin/gzip -9 -f $binfile"                  ) == 0 || die "Error in zip $binfile \n";
                system("cd $cwd && /bin/mv -f $binfile.gz $mds_ldir/$the_year") == 0 || die "Error in mv $binfile.gz\n";
                return "$mds_ldir/$the_year/$binfile.gz";
             }
             else
             {
                return undef;
             }
          }
          # Others LAI and VEG
          else
          {
             #Min Xu change to avoid the partily download files
             #&wget_modis($mds_durl, "$mds_ldir/$the_year");
             #@temp = glob("$mds_ldir/$the_year/$hdf_star");  
             $hdf_data = wget_modis($mds_durl, $cwd);
             #@temp = glob("$hdf_star");  
             print {$LOG} "DownMDS_PP: $hdf_star, $cwd \n";
             #if($#temp >=0) 
             if(defined $hdf_data)
             {
                #system("cd $cwd && mv -f $temp[0] $mds_ldir/$the_year") ==0 || die "Error in mv $temp[0]\n"; 
                #return "$mds_ldir/$the_year/$temp[0]";
                system("cd $cwd && /bin/mv -f $hdf_data $mds_ldir/$the_year") ==0 || die "Error in mv $hdf_data\n"; 
                return "$mds_ldir/$the_year/$hdf_data";
             }
             else
             {
                return undef;     
             }
          }
      }


      # dowload data from LPDAAC data pool using wget
      sub wget_modis($$)
      {
          my $mdsurl;
          my $runcmd;
          my $locdir;

          $mdsurl = shift;
          $locdir = shift;

          my $cwd = cwd();

          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};

          my ($mdsdir, $mdsprx, $output, $mdskey);

          $mdsurl =~/(.*)\/(.*)\*\.hdf/;
          $mdsdir = $1."/"; $mdsprx = $2; $mdsprx =~ /(.*)\.h/; $mdskey = $1; 

          if(! defined $RmdsGBL{$mdskey}) 
          { 
             my @dirlst;
             print {$LOG} "xxxxx $mdsdir, $mdsprx, $locdir, $mdskey \n";
             print {$LOG} "curl -l $mdsdir -- $#dirlst\n";

             # since LPDAAC has changed from ftp to HTTP more wrok need to get down
             #@dirlst = `curl -l -C -  --retry 10 --retry-delay 60 $mdsdir -o $cwd/$mdskey.look.dat`;
 
             @dirlst = `curl -l -C -  --retry 30 --retry-delay 120 $mdsdir`;

             open(FO, ">$cwd/$mdskey.look.dat") || die "cannot open $mdskey.look.dat for dump $!\n";
             foreach my $htmlCode (@dirlst)
             {
                 $htmlCode =~ s/<.+?>//g;   # remove html tags
                 if(($htmlCode =~ /^\s*MCD/ || $htmlCode =~ /^\s*MOD/ || $htmlCode =~ /^\s*MYD/) && ! ($htmlCode =~ /xml/))   # remove xml and other lines
                 {
                     $htmlCode =~ s/hdf.+/hdf/g;   # remove time stamp and data size
                     $htmlCode =~ s/^\s*//g;       # remove head space
                     print FO  $htmlCode;
                 }
             }
             close(FO);

             @dirlst =();
             open(FH, "$cwd/$mdskey.look.dat") || die "cannot open $mdskey.look.dat for read $!\n";
             @dirlst = <FH>;
             print {$LOG} "xxxxx---, $#dirlst \n";
             $RmdsGBL{$mdskey} = \@dirlst;
             close(FH);
             #unlink "$mdskey.look.dat";
          }

          undef $output;
          foreach my $list (@{$RmdsGBL{$mdskey}})
          {
             my $schstr = quotemeta $mdsprx;

             chomp($list);
             if($list=~/\.hdf$/ && $list=~/$schstr/)
             {
                 $output = $list;
             }
          }

          if(! defined $output)
          {
               print {$LOG} "cannot find $mdsprx in ftp directory \n";
               return $output;

          }
          #$output = `curl -l $mdsdir | grep \.hdf\$ | grep -i $mdsprx`;

          #$runcmd = "wget --no-remove-listing -c -t 10 --directory-prefix=$locdir \"$mdsurl\"";
          $runcmd = "cd $locdir && curl -O -C - --retry 30 --retry-delay 120 -S -s $mdsdir$output";

          my $numtry = 30;
          while(not (system($runcmd) == 0) and $numtry > 0)
          {
             print {$LOG} "$runcmd \n";
             $numtry--;
             sleep 120;
          }

          print "ERR:$!: $runcmd \n" if($numtry == 0);
          #system($runcmd) == 0 || print "ERR:$!: $runcmd \n";

          return $output;
      }



      sub PrepareDirs_old()
      {
          print "\n\nPreparing the directory structures for iCWPS-MODIS\n";

          # modis hdf or preprocessed binary data
          mkdir "$PREFIX/MODIS"                       if(! -d "$PREFIX/MODIS"                );
          mkdir "$PREFIX/MODIS/ALB"                   if(! -d "$PREFIX/MODIS/ALB"            );
          mkdir "$PREFIX/MODIS/LAI"                   if(! -d "$PREFIX/MODIS/LAI"            );
          mkdir "$PREFIX/MODIS/VEG"                   if(! -d "$PREFIX/MODIS/VEG"            );

          # 1 km data directory
          mkdir "$PREFIX/iCWPS_1km"                   if(! -d "$PREFIX/iCWPS_1km"            );
          mkdir "$PREFIX/iCWPS_1km/ALB/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/ALB/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/LAI/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/LAI/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/VEG/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/VEG/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/FVC/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/FVC/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/MVI/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/MVI/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/HVI/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/HVI/$CASENM");
          mkdir "$PREFIX/iCWPS_1km/OPT/$CASENM"       if(! -d "$PREFIX/iCWPS_1km/OPT/$CASENM");

          # n km data directory
          mkdir "$PREFIX/iCWPS_nkm"                   if(! -d "$PREFIX/iCWPS_nkm"            );
          mkdir "$PREFIX/iCWPS_nkm/ALB/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/ALB/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/LAI/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/LAI/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/VEG/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/VEG/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/FVC/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/FVC/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/MVI/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/MVI/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/HVI/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/HVI/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/OPT/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/OPT/$CASENM");
          mkdir "$PREFIX/iCWPS_nkm/SOM/$CASENM"       if(! -d "$PREFIX/iCWPS_nkm/SOM/$CASENM");
      
          # working directoy
          mkdir "$PREFIX/wk"                          if(! -d "$PREFIX/wk"                   );
          mkdir "$PREFIX/wk/ALB"                      if(! -d "$PREFIX/wk/ALB"               );
          mkdir "$PREFIX/wk/LAI"                      if(! -d "$PREFIX/wk/LAI"               );
          mkdir "$PREFIX/wk/VEG"                      if(! -d "$PREFIX/wk/VEG"               );
          mkdir "$PREFIX/wk/FVC"                      if(! -d "$PREFIX/wk/FVC"               );
          mkdir "$PREFIX/wk/MVI"                      if(! -d "$PREFIX/wk/MVI"               );
          mkdir "$PREFIX/wk/HVI"                      if(! -d "$PREFIX/wk/HVI"               );
          mkdir "$PREFIX/wk/SOM"                      if(! -d "$PREFIX/wk/SOM"               );
          mkdir "$PREFIX/wk/OPT"                      if(! -d "$PREFIX/wk/OPT"               );
      }

      sub PrepareDirs()
      {
          print "\n\nPreparing the directory structures for iCWPS-MODIS\n\n";
          foreach (@_) {mkpath($_)};
          return 0;
      }


      # parse the namelist section to get the item values
      sub ParseNML($)
      {
          my ($item);

          my ($line, $nmli);

          $item = shift;

          undef $nmli;

          open(FL, "$iCWPSH/for2/namelist.icwps-modis") || die "\ncan't open nml.icwps.modis\n";

          while($line=<FL>)
          {
              if($line=~/$item(.*)/)
              {
                 $nmli = $1;
                 if($nmli)
                 {
                    chomp($nmli);
                    $nmli =~s/\s*,*\s*$//g;     # remove , space
                    $nmli =~s/^\s*=\s*//g;      # remove = space
                    $nmli =~s/\'*//g;           # remove '
                    $nmli =~s/\"*//g;           # remove "

                    if($nmli ne '')
                    {
                       close(FL);
                       return($nmli);
                       last; 
                    }
                    else
                    {

                       close(FL);
                       print "\nERR: Please set $item !!!\n\n";
                       exit 1;
                    }
                 }
                 else
                 {
                    close(FL);
                    print "\nERR: Please set $item !!!\n\n";
                    exit 1;
                 }
              }
          }

      }

      # Domain determination
      sub DomainSet
      {
          my $dirwk = shift;
          my ($nwlat, $nwlon, $selat, $selon, $crlat, $crlon);
          my ($ihs, $ihe, $jvs, $jve, $out, @tmp);

          my (%list_jv, @list_ih);

          my (%ihs0, %ihe0);

          #my $output = `$RmapPRG domain`;

          print("cd $dirwk && $RmapPRG domain MCD15A2.A hq 1974200 >& /dev/null");

          if(system("cd $dirwk && $RmapPRG domain MCD15A2.A hq 1974200 >& /dev/null") == 0) 
          {$out = `cd $dirwk && $RmapPRG domain MCD15A2.A hq 1974200`;}
          else       
          {die "\n\n Err: in run $RmapPRG\n\n"  ;}
 

          $out =~ /\s*(\S+)\s*(\S+)\s*(\S+)\s*(\S+)/;
          print "\nDomain: $1, $2, $3, $4\n";
          ($nwlat, $nwlon, $selat, $selon) = ($1, $2, $3, $4);

          $crlat=$nwlat + 1;

          #xum fix the east and west boundary error
          $nwlon = $nwlon - 1.;
          $selon = $selon + 1.;

          $nwlon = -179.999 if($nwlon <  -180.);
          $selon =  179.999 if($selon >   180.);

          %ihs0 = ();
          %ihe0 = ();

          while($crlat >= $selat)
          {
             ($ihs, $jvs) = &get_tile_id($crlat, $nwlon);
             ($ihe, $jve) = &get_tile_id($crlat, $selon);

             print "ERR: $jvs not equal $jve \n" if($jvs != $jve);

             if(!defined $ihs0{$jvs}) {$ihs0{$jvs} = $ihs;}
             if(!defined $ihe0{$jvs}) {$ihe0{$jvs} = $ihe;}

             if(defined $ihs0{$jvs} && $ihs < $ihs0{$jvs}) {$ihs0{$jvs} = $ihs;}
             if(defined $ihe0{$jvs} && $ihe > $ihe0{$jvs}) {$ihe0{$jvs} = $ihe;}
             $crlat = $crlat - 1./110.;

          }

          foreach $jvs (sort{$a <=> $b} keys %ihs0)
          {
             @list_ih = ();
             @list_ih = ($ihs0{$jvs} .. $ihe0{$jvs});
             $list_jv{$jvs} = join(',', @list_ih);
             print "$jvs -- @list_ih \n";
          }

          #+xum to fix the south bounday bug.
          #
          ($ihs, $jvs) = &get_tile_id($selat-1, $nwlon);
          ($ihe, $jve) = &get_tile_id($selat-1, $selon);
          print "ERR: $jvs not equal $jve \n" if($jvs != $jve);
          if(defined $list_jv{$jvs})
          {
             @tmp = split(',',$list_jv{$jvs});
             $ihs = $tmp[0]     if($ihs > $tmp[0]);
             $ihe = $tmp[$#tmp] if($ihe < $tmp[$#tmp]);
          }
          @list_ih = ();
          @list_ih = ($ihs .. $ihe);
          $list_jv{$jvs} = join(',', @list_ih);
          #=xum

          return %list_jv;
      }

      # Use LDOPE tool to get the IDs
      sub get_tile_id($$)
      {
          my ($cnrlat, $cnrlon);    # unit is degree

          my ($ihtile, $jvtile, $result);
          $cnrlat = shift;
          $cnrlon = shift;

          $result = `$TilePRG -proj=SIN -lat=$cnrlat -lon=$cnrlon`;

          $result =~/:\s+(\d+)\s+(\d+)/;

          $ihtile = $1;
          $jvtile = $2;

          if(defined $ihtile && defined $jvtile) {return($ihtile, $jvtile);}
          else                                   {die "\n\nErr: Error in run $TilePRG \n\n";}
      }


      sub BlankSpacesdsname($)
      {
          my $sdsname = shift;
          my (@temp, $item, $mskname);

          @temp = split(',', $sdsname);

          $mskname = "";

          foreach $item (@temp)
          {
             $item =~ s/^\s*//g;
             $item =~ s/\s*$//g;
             $item =~ s/\.1-3//g;
             if($item eq $temp[$#temp]) {$mskname .= "\'$item\'";}
             else                       {$mskname .= "\'$item\',";}
          }

          return $mskname;

      }


      sub proc_gds_sub($$$$$$)
      {
          my ($tmref, $diref, $DmonSom, $D3hrSOM) = @_; 

          my ($cy, $im, $cm, $jd, $cd, $ih, $ch);
          my ($fnGRB, $fnSOM, $fnTMP, @temp);

          my ($prcyr, $jdstr, $jdstp, $jdinv) = @$tmref;
          my ($dirsv, $dr1km, $drnkm, $dirwk) = @$diref;

          my $cwd = cwd();
          chdir $dirwk;

          my $thr = threads->self();
          my $tid = $thr->tid();

          $cy = $prcyr;


          print "xxxxxxx, $dirwk $DmonSOM $D3hrSOM \n";

          if(! $DmonSOM)
          {
             foreach $im ((1..12))
             {
                 $cm = sprintf("%02d", $im);
                 $fnGRB = 'GLDAS_NOAH10_M_E1.A'.$cy.$cm.'.002.grb';

                 $fnSOM = $fnGRB;
                 $fnTMP = $fnGRB;
                 $fnSOM =~s/\.grb/\.$domNAME\.bin/;
                 $fnTMP =~s/\.grb/\.bin/;

                 print "xxx wget -c -q $FtpGDAS/$cy/$fnGRB \n\n";

                 system("wget -c -q $FtpGDAS/$cy/$fnGRB") == 0 || die "Error in wget for GLDAS \n\n";
                 system("$ConvPRG \'FNAME=\"$fnGRB\"\' ncl_sds2bin_som.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";
                 rename $fnTMP, $fnSOM;
             }
          }
          if(! $D3hrSOM)
          {
             for($jd=$jdstr; $jd<=$jdstp; $jd+=$jdinv)
             {
                 $cd = sprintf("%03d", $jd);

                 for($ih=0; $ih <24; $ih+=3)
                 {
                     $ch = sprintf("%02d", $ih);
                     $fnGRB = "GLDAS_NOAH10_3H_E1.A".$cy.$cd.".$ch"."00.002.*.grb";

                    
                     $fnTMP = $fnGRB;
                     $fnTMP =~s/\.grb/\.bin/;
                     $fnSOM = "GLDAS_NOAH10_3H_E1.A".$cy.$cd.".$ch"."00.002.$domNAME.bin";;
                     next if(-e "$dirwk/$fnSOM");

                     my $FtpG3hr = "ftp://hydro1.sci.gsfc.nasa.gov/data/s4pa/GLDAS/GLDAS_NOAH10_3H_E1.002/";
                     my $numtry = 30;
                     @temp = glob("$dirwk/$fnGRB");
                     print "$tid .... $#temp \n";
                     while($#temp < 0 and $numtry > 0)
                     {
                         system("cd $dirwk && wget -c -q $FtpG3hr/$cy/$cd/$fnGRB") == 0 || print "ERR: in wget $?\n";
                         last if($#temp >=0);
                         $numtry--;
                         print "trying .. $tid : $#temp $FtpG3hr/$cy/$cd/$fnGRB in number $numtry\n";
                         @temp = glob("$dirwk/$fnGRB");
                         sleep 10;
                     }
                     #system("cd $dirwk && wget -c -q -t 10 --waitretry=30 ftp://hydro1.sci.gsfc.nasa.gov/data/s4pa/GLDAS/GLDAS_NOAH10_3H_E1.002/$cy/$cd/$fnGRB") == 0 || 
                     #                  die "Error in wget for GLDAS \n\n";
                     if($#temp >= 0)
                     {
                        $fnGRB = basename($temp[0]);

                        system("cd $dirwk && $ConvPRG \'FNAME=\"$fnGRB\"\' ncl_sds2bin_som.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";
                        system("cd $dirwk && /bin/mv $fnTMP $fnSOM") == 0 || print "ERR: SOM mv \n";
                     }
                     else
                     {
                         print "missing .. $fnGRB \n"
                     }
                 }
             }
          }

          chdir $cwd;
          return 'gds';
      } 


      sub proc_mds_sub($$$$$$$$)
      {

          my ($tmref, $diref, $mdsnm, $tiles, $mdsvr, $mskpg, $sdsnm, $mskop) = @_;
          my (@fnHDf, $mdsen, $jd, $prcdy);

          my ($prcyr, $jdstr, $jdstp, $jdinv) = @$tmref;
          my ($dirsv, $dr1km, $drnkm, $dirwk) = @$diref;

          if   ($mdsnm =~ /MCD/) { $mdsen = 'combined'; }
          elsif($mdsnm =~ /MYD/) { $mdsen = 'aqua';     }
          elsif($mdsnm =~ /MOD/) { $mdsen = 'terra';    }
          else                   { print "MODIS product $mdsnm not found \n"; exit;}

          my $cwd = cwd();
          chdir $dirwk;

          my $key = $MdsProd{$mdsnm}->[0];
          my $thr = threads->self();
          my $tid = $thr->tid();


          my $logfile = "${$LogHash{$key}}[0].".sprintf("%d", $tid);
          open(my $FH, ">$logfile") || die "cannot open file $logfile \n";
          $LogsGBL{$tid} = $FH;
          my $LOG = $LogsGBL{$tid};


          print "deb == $tid === --$key--- $LogsGBL{$tid}\n";
          print {$LOG} $key."-threads: ".$thr->tid()."\n";
          print {$LOG} "xxxdeb, $dirwk, $cwd \n";

          if($mdsnm =~ /43B/)
          {
              $NsdsGBL = $sdsnm;
              $MaskGBL = $mskop;
              $MprgGBL = $mskpg;
          }

          for($jd=$jdstr; $jd<=$jdstp; $jd+=$jdinv)
          {
              $prcdy = sprintf("%03d", $jd);
              my $tstmp = $prcyr.$prcdy;

              #xum first to test if file already in 1km directory, 
              my @temp = glob("$dr1km/$mdsnm$tstmp*");
              if($#temp >= 0) {print {$LOG} @temp; next;}
              
              undef $RmdsGBL{$mdsnm.$tstmp};
              @fnHDF = &gen_modis_filenames($prcyr, $prcdy, $dirsv, $mdsnm, $tiles, $mdsvr, $mdsen);
              &genrecordsNmask($dirwk, $mskpg, $sdsnm, $mskop, @fnHDF);

              # re-projection
              system("cd $dirwk && $RmapPRG near $mdsnm hq $tstmp $dr1km > log.$prcyr") == 0 || die "error in remap \n";
              #unlink $mdsnm.$tstmp.'.records';

              #do not remove the binary file
              #if($mdsnm =~ /43B/) {system("cd $dirwk && /bin/rm -f $mdsnm$tstmp*.bigBIN") == 0 || die "error in remove \n";}
              #else                {system("cd $dirwk && /bin/rm -f $mdsnm$tstmp*.bin"   ) == 0 || die "error in remove \n";}

              #+xum, in one situation that all data from pool not from ftp
              #      then the $RmdsGBL is undef

              if(defined $RmdsGBL{$mdsnm.$tstmp})
              {
                 #my @dir = @{$RmdsGBL{$mdsnm.$tstmp}}; 
                 #for(my $i=0; $i<=$#dir; $i++) {delete ${$RmdsGBL{$mdsnm.$tstmp}}[$i];}

                 @{$RmdsGBL{$mdsnm.$tstmp}} =();
                 delete $RmdsGBL{$mdsnm.$tstmp} ;
              }
          }

          close($FH);
          chdir $cwd;
          print "in sub $MdsProd{$mdsnm}->[0]\n";
          return $MdsProd{$mdsnm}->[0];
      }

      #masking
      #----------------------------------------------------------------------------------------------------------------------------
      sub mask_data_mvi($$$)
      {
          my ($sdsnam, $hdfinp, $maskqc);
          my ($hdfqua, $hdfout, $qalout, $binout, $mskcmd, $runcmd, $qalcmd, $sdsncl, $sdsmsk);

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          my $cwd = cwd();

          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};

          $sdsncl = &CHGsdsname($sdsnam);
          $sdsmsk = &BlankSpacesdsname($sdsnam);

          #$hdfout = 'MDmvi.hdf';
          #$qalout = 'MDqal.hdf';
          $hdfout = $hdfinp; $hdfout =~s/hdf/msk.hdf/;
          $qalout = $hdfinp; $qalout =~s/hdf/qal.hdf/;

          $hdfqua = $hdfinp;
          $mskcmd = "\"$hdfqua,$maskqc\"";

          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsmsk                          -fill=-3000 -meta -mask=$mskcmd $hdfinp";
          $qalcmd = "$MaskPRG -of=$qalout -sds='1 km 16 days pixel reliability' -fill=-1    -meta -mask=$mskcmd $hdfinp";

          print {$LOG} "xumdeb, $runcmd \n";
          system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";
          system("cd $cwd && $qalcmd >& /dev/null") == 0 || print "ERR:$!: $qalcmd \n";

          $binout = $hdfinp;
          $binout =~s/hdf/bin/;
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'QNAME=\"$qalout\"\' \'s=$sdsncl\' ncl_sds2bin_mvi.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";

          #rename 'MDmvi.bin', $binout;
          #unlink 'MDmvi.hdf';
          #unlink 'MDqal.hdf';
          unlink $hdfinp;
          return $binout;
      }

      sub mask_data_hvi($$$)
      {
          my ($sdsnam, $hdfinp, $maskqc);
          my ($hdfqua, $hdfout, $qalout, $binout, $mskcmd, $runcmd, $qalcmd, $sdsncl, $sdsmsk);

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          my $cwd = cwd();

          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};

          $sdsncl = &CHGsdsname($sdsnam);
          $sdsmsk = &BlankSpacesdsname($sdsnam);

          $hdfout = $hdfinp; $hdfout =~s/hdf/msk.hdf/;
          $binout = $hdfinp;
          $binout =~s/hdf/bin/;

          if($maskqc == 'None' or $maskqc)
          {
            print ("cd $cwd && $ConvPRG \'FNAME=\"$hdfinp\"\' \'s=$sdsncl\' ncl_sds2bin_hvi.ncl >& /dev/null");
            system("cd $cwd && $ConvPRG \'FNAME=\"$hdfinp\"\' \'s=$sdsncl\' ncl_sds2bin_hvi.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n"; 
          }
          else
          {
             $qalout = $hdfinp; $qalout =~s/hdf/qal.hdf/;

             $hdfqua = $hdfinp;
             $mskcmd = "\"$hdfqua,$maskqc\"";

             $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsmsk                          -fill=-3000 -meta -mask=$mskcmd $hdfinp";
             $qalcmd = "$MaskPRG -of=$qalout -sds='1 km 16 days pixel reliability' -fill=-1    -meta -mask=$mskcmd $hdfinp";

             print {$LOG} "xumdeb, $runcmd \n";
             system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";
             system("cd $cwd && $qalcmd >& /dev/null") == 0 || print "ERR:$!: $qalcmd \n";
             system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'QNAME=\"$qalout\"\' \'s=$sdsncl\' ncl_sds2bin_mvi.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";
          }

          #rename 'MDmvi.bin', $binout;
          #unlink 'MDmvi.hdf';
          #unlink 'MDqal.hdf';
          #unlink $hdfinp;
          return $binout;
      }




      sub mask_data_lai($$$)
      {
          my ($sdsnam, $hdfinp, $maskqc);
          my ($hdfqua, $hdfout, $binout, $mskcmd, $runcmd, $sdsncl);

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          my $cwd = cwd();

          $sdsncl = &CHGsdsname($sdsnam);

          $hdfout = $hdfinp;
          $hdfout =~s/hdf/msk.hdf/;

          $hdfqua = $hdfinp;
          $mskcmd = "\"$hdfqua,$maskqc\"";

          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsnam -fill=255 -meta -mask=$mskcmd $hdfinp";

          system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";

          $binout = $hdfinp;
          $binout =~s/hdf/bin/;
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'s=$sdsncl\' ncl_sds2bin_lai.ncl >& /dev/null") == 0 || print "ERR:$?: $ConvPRG \n";

          system("/bin/rm -f $cwd/$hdfout") == 0 || die "failed on $hdfout: $? \n";
          system("/bin/rm -f $cwd/$hdfinp") == 0 || die "failed on $hdfout: $? \n";
          return $binout;
      }


      #----------------------------------------------------------------------------------------------------------------------------
      sub mask_data_veg($$$)
      {
          my ($hdfinp, $sdsnam, $maskqc);

          my ($hdfqua, $hdfout, $binout);
          my ($mskcmd, $runcmd);
          my ($sdsncl);

          my $cwd = cwd();

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          #$MSKveg = 'Land_Cover_Type_QC,0-1==00';

          $sdsncl = &CHGsdsname($sdsnam);

          #$hdfout = 'MDveg.hdf';

          $hdfout = $hdfinp;
          $hdfout =~s/hdf/msk.hdf/;

          $hdfqua = $hdfinp;

          $binout = $hdfinp;
          $binout =~s/hdf/bin/;

          $mskcmd = "\"$hdfqua,$maskqc\"";

          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsnam -fill=255 -meta -mask=$mskcmd $hdfinp";


          system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'s=$sdsncl\' ncl_sds2bin_veg.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";

          my $hdfdat = $binout;
          $hdfdat =~s/bin/dat/;
          system("/bin/rm -f $cwd/$hdfdat") == 0 || die "failed in $hdfdat:$? \n";
          system("/bin/rm -f $cwd/$hdfout") == 0 || die "failed in $hdfout:$? \n";
          system("/bin/rm -f $cwd/$hdfinp") == 0 || die "failed in $hdfinp:$? \n";

          return $binout;
      }


     sub mask_data_bgc($$$)
      {
          my ($hdfinp, $sdsnam, $maskqc);

          my ($hdfqua, $hdfout, $binout);
          my ($mskcmd, $runcmd);
          my ($sdsncl);

          my $cwd = cwd();

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          $sdsncl = &CHGsdsname($sdsnam);

          $hdfout = $hdfinp;
          $hdfout =~s/hdf/msk.hdf/;

          $hdfqua = $hdfinp;

          $binout = $hdfinp;
          $binout =~s/hdf/bin/;

          $mskcmd = "\"$hdfqua,$maskqc\"";

          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsnam -fill=65535 -meta -mask=$mskcmd $hdfinp";


          system("cd $cwd && $runcmd >& /dev/null") == 0 || die "ERR:$!: $runcmd \n";
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'s=$sdsncl\' ncl_sds2bin_bgc.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";

          my $hdfdat = $binout;
          $hdfdat =~s/bin/dat/;
          system("/bin/rm -f $cwd/$hdfdat") == 0 || die "failed in $hdfdat:$? \n";
          system("/bin/rm -f $cwd/$hdfout") == 0 || die "failed in $hdfout:$? \n";
          system("/bin/rm -f $cwd/$hdfinp") == 0 || die "failed in $hdfinp:$? \n";

          return $binout;
      }


      sub mask_data_vcf($$$)
      {
          my ($hdfinp, $sdsnam, $maskqc);

          my ($hdfqua, $hdfout, $binout);
          my ($mskcmd, $runcmd);
          my ($sdsncl);

          my $cwd = cwd();

          $hdfinp = shift;
          $sdsnam = shift;
          $maskqc = shift;

          #$MSKveg = 'Land_Cover_Type_QC,0-1==00';

          $sdsncl = &CHGsdsname($sdsnam);

          #$hdfout = 'MDveg.hdf';

          $hdfout = $hdfinp;
          $hdfout =~s/hdf/msk.hdf/;

          $hdfqua = $hdfinp;

          $binout = $hdfinp;
          $binout =~s/hdf/bin/;

          $mskcmd = "\"$hdfqua,$maskqc\"";

          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsnam -fill=255 -meta -mask=$mskcmd $hdfinp";


          if($maskqc=~/-/)
          {
             print "deb copy \n";
             system("cd $cwd && /bin/cp -f $hdfinp $hdfout") == 0 || print "ERR: in copy \n";
          }
          else
          {
             system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";
          }
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'s=$sdsncl\' ncl_sds2bin_vcf.ncl >& /dev/null") == 0 || print "ERR:$!: $ConvPRG \n";

          my $hdfdat = $binout;
          $hdfdat =~s/bin/dat/;
          system("/bin/rm -f $cwd/$hdfdat") == 0 || die "failed in $hdfdat:$? \n";
          system("/bin/rm -f $cwd/$hdfout") == 0 || die "failed in $hdfout:$? \n";
          system("/bin/rm -f $cwd/$hdfinp") == 0 || die "failed in $hdfinp:$? \n";

          return $binout;
      }


      sub mask_data_alb($$$$$)
      {
          my ($wrkdir, $hdfinp, $hdfqua, $sdsnam, $maskqc);

          my ($binout, $hdfout, $szaout, $mskcmd, $runcmd, $upkcmd, $sdsncl); 
          
          $wrkdir = shift;
          $hdfinp = shift;
          $hdfqua = shift;
          $sdsnam = shift;
          $maskqc = shift;

          #my $cwd = cwd();
          my $cwd = $wrkdir;
          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};


          my @lctime = localtime();
          my $curtim = sprintf("%4d%03d", $lctime[5]+1900, $lctime[7]);

          my @filstr = split(/\./, $hdfinp);

          #my @temp   = glob("MCD43B2.$filstr[1].$filstr[2].$filstr[3]*.hdf");
          #
          #if($#temp < 0) {die "\n no quality for $hdfinp\n";}
          #else
          #{
          #   # avoid the many data, choose the largest one
          #   $hdfqua = $temp[0];
          #   foreach $a (@temp)
          #   {
          #      if((-s $hdfqua) < (-s $a))
          #      {
          #         $hdfqua = $a;
          #      }
          #   }
          #}

          
          #replace the 4 and 5 part
          $filstr[4] = 'BroadBand3.'.$curtim;
          $filstr[5] = 'sin.bigBIN';

          $sdsncl    = &CHGsdsname($sdsnam);

          $binout    = join('.', @filstr[0..5]);

          #$hdfout    = 'MDalb.hdf';
          #$szaout    = 'LNsza.hdf';

          $hdfout = $hdfinp; $hdfout =~s/hdf/msk.hdf/;
          $szaout = $hdfqua; $szaout =~s/hdf/qal.hdf/;

          my $chktime = 0;
          if(not (-e $hdfqua and -e $hdfinp))
          {
              sleep 10;
              $chktime = $chktime + 10;
          }
          else
          {
              $chktime = 0;
          }

          if($chktime >= 300)
          {
              die "not such files $hdfqua and $hdfinp \n";
          }

          $mskcmd = "\"$hdfqua,$maskqc\"";
          $runcmd = "$MaskPRG -of=$hdfout -sds=$sdsnam -fill=32767 -meta -mask=$mskcmd $hdfinp";

          $upkcmd = "$UnpkPRG -of=$szaout -sds=BRDF_Albedo_Ancillary -bit=8-14 -meta $hdfqua";

         


          system("cd $cwd && $runcmd >& /dev/null") == 0 || print "ERR:$!: $runcmd \n";
          system("cd $cwd && $upkcmd >& /dev/null") == 0 || print "ERR:$!: $upkcmd \n";

          #system("$ConvPRG \'s=$sdsncl\' ncl_sds2bin_alb.ncl >& /dev/null") == 0 || print "ERR:$?: $ConvPRG \n";
          print {$LOG} "yyyy $ConvPRG \'s=$sdsncl\' ncl_sds2bin_alb.ncl \n";
          system("cd $cwd && $ConvPRG \'FNAME=\"$hdfout\"\' \'QNAME=\"$szaout\"\' \'s=$sdsncl\' ncl_sds2bin_alb.ncl > /dev/null") == 0 || print "ERR:$?: $ConvPRG \n";

          my $hdfbin = $hdfinp; $hdfbin=~s/hdf/bin/;
          #rename $hdfbin, $binout;

          
          
          #while( (-s $hdfout) != (-s $binout) )
          #{
          #    sleep 30;

          #    my $s2 = -s $cwd/$binout;
          #}

   
          #my $s1 = -s $hdfbin;
          #while($s1 < 4*10*1200*1200)
          #{
          #   print "size1: $s1 $hdfbin\n";
          #   sleep 10;
          #   $s1 = -s $hdfbin;
          #   print "size2: $s1 $hdfbin\n";
          #}
          system("cd $cwd && /bin/mv -f $hdfbin $binout") == 0 || die "cannot move $binout\n";


          system("/bin/rm -f $cwd/$hdfinp") == 0 || die "delete failed in $hdfinp:$? \n";
          system("/bin/rm -f $cwd/$hdfqua") == 0 || die "delete failed in $hdfqua:$? \n";
          system("/bin/rm -f $cwd/$hdfout") == 0 || die "delete failed in $hdfout:$? \n";
          system("/bin/rm -f $cwd/$szaout") == 0 || die "delete failed in $szaout:$? \n";

          return $binout;
      }

      sub CHGsdsname($)
      {
          my $sdsname = shift;
          my (@temp, $item, $nclname);

          @temp = split(',', $sdsname);

          #$nclname = "\'(\/";
          $nclname = "(\/";
  
          foreach $item (@temp)
          {
             $item =~ s/^\s*//g;
             $item =~ s/\s*$//g;
             $item =~ s/\.1-3//g;

             # Min add for the space replaced by underline
             $item =~ s/ /_/g;
             if($item eq $temp[$#temp]) {$nclname .= "\"$item\"";}
             else                       {$nclname .= "\"$item\",";}
          }

          #$nclname .= "\/)\'";
          $nclname .= "\/)";

          return $nclname;

      }

      # get filenames
      # return the list of hdf/bin files in the working directory
      sub gen_modis_filenames($$$$$$$)
      {
          use File::Basename;

          my ($cyear, $juldy, $dirnm, $basnm, $refer, $versn, $sense);

          my (@lstih, %lstjv, @lstfn);
          my ($ih, $jv, $id, $fn, $fx, $fb, $fh, $fu);

          $cyear = shift;  
          $juldy = shift;  
          $dirnm = shift;
          $basnm = shift;      # attn: including ".A"
          $refer = shift;      # tile list covering the whole domain
          $versn = shift;
          $sense = shift;

          @lstfn = ();
          %lstjv = %$refer;

          my $cwd = cwd();
          my $thr = threads->self();
          my $tid = $thr->tid();
          my $LOG = $LogsGBL{$tid};

          print "$tid -> $LOG ---xxxx\n";

          print {$LOG} "$basnm, $cwd\n";

          foreach $jv (sort{$a <=> $b}(keys %lstjv))
          {
             @lstih = split(',',$lstjv{$jv});

             foreach $ih (@lstih)
             {
                $id = sprintf("h%02dv%02d", $ih, $jv);

                if($basnm =~ /43B/)
                {
                   $fn = $basnm.$cyear.$juldy.".".$id."*.bigBIN.gz";
                }
                else
                {
                   $fn = $basnm.$cyear.$juldy.".".$id."*.hdf";
                }


                $fx = &inquire_fn($dirnm, $fn, $cyear);

                # $fx is the full path of the file
                # basename($fx) is remove the directroy information

                $fh = $basnm.$cyear.$juldy.".".$id."*.hdf";

                # xum: check integrity
                if(defined $fx and (not basename($fx) =~/.gz/))
                {
                   if(system("ncl_filedump -c $fx >& /dev/null") != 0)
                   {
                       print {$LOG} "xxx file broken, need to redownload $fx \n";
                       undef $fx;
                   }
                }
                # --------------------------

                if(defined $fx)
                {
                   $fb = basename($fx);
                   print {$LOG} "$tid Obtaining $fb from Data Directory to $cwd  \n";
                   $fu = $fb;
                   $fu =~s/.gz//;

                   print {$LOG} "xxx $basnm, $cwd\n";
                   system("/bin/cp -f $fx $cwd")            == 0 || die "failed   cp $fx: $!\n" if((! -e $fb)  && (! -e $fu));
                   system("cd $cwd && /bin/gzip -d -f $fb") == 0 || die "failed gzip $fb: $?\n" if($fb =~/.gz/ && (! -e $fu)); 
                   push @lstfn, $fu;     # used for the FORTRAN codes, normally in current working directory
                }
                else
                {
                   #+xum add ocean tils
                   #next if(defined $is_ocn{$id});
                   if(defined $is_ocn{$id})
                   {
                      push @lstfn, "OCN$id.fake.hdf";
                   }
                   else
                   {
                      $fx = &DownMDS_PP($cwd, $fh, $cyear, $juldy, $dirnm, $basnm, $versn, $sense);
                      if(defined $fx)
                      {
                         $fb = basename($fx);
                         print {$LOG} "Obtaining $fb from LPDACC FTP to $cwd \n";
                         $fu = $fb;
                         $fu =~s/.gz//;
                         system("/bin/cp -f $fx $cwd") if((! -e $fb)  && (! -e $fu));
                         system("cd $cwd && /bin/gzip -d $fb" ) if($fb =~/.gz/ && (! -e $fu)); 
                         push @lstfn, $fu;     # used for the FORTRAN codes, normally in current directory
                      }
                      else
                      {
                         print {$LOG} "please check the tile $id to see if it is ocean tiles, if so, add it in script\n";
                         push @lstfn, "DNO$id.fake.hdf";
                      }
                   }
                }
             }
          }
          return @lstfn;
      }

      # check filename and file existence.
      # return the first full file name with the $file, else undef
      sub inquire_fn($$$)
      {
          my ($mdir, $file, $year, @fils);
          $mdir = shift; $file = shift; $year = shift;

          if   ($file=~/MOD15A2/) {@fils = glob($mdir."/$year"."a"."/$file");}
          else                    {@fils = glob($mdir."/$year/$file");}

          #print $mds_lai_dir."/$year/$file", "deb @fils \n";

          return($fils[0]) if($#fils >= 0);

          return undef;
      }


      # time libs
      sub isleap
      {
          my $year; my $isleapyr;

          $year = shift;
          $isleapyr = 0;
          if(0 == $year % 4 and 0 != $year % 100 or 0 == $year % 400) {$isleapyr = 1;}

          return $isleapyr;
      }

      # Julian day to month and day in month
      sub jd2mndy
      {
          my ($im, $id, $iy, $jd);

          $iy = shift;
          $jd = shift;

          my @edys = (0,31,28,31,30,31,30,31,31,30,31,30,31);

          my $cday = 0.;

          $im = 0;

          while($jd>$cday)
          {
             $im   = $im + 1;
             $id   = $jd - $cday;;
             if($im == 2 && isleap($iy) == 1)
             {
                $cday = $cday + $edys[$im] + 1;
             }
             else
             {
                $cday = $cday + $edys[$im];
             }
          }

          return ($im, $id);
      }

      #+xum generate the records file used by REMAPROJ program
      sub genrecordsNmask
      {
          my ($WorkDir, $MaskFun, $NsdsMDS, $MaskMDS, @fnHDF) = @_; 

          my ($fnLST, $fnFIL, $fnREC);
          print @fnHDF, "\n";

          foreach $fnLST (@fnHDF)
          {
             if($fnLST!~/^OCN/ && $fnLST!~/^DNO/)
             {
                 $fnLST =~/(.*)h\d+v\d+/;
                 $fnREC = $1."records";
                 last;
             }

          }
          print 'xxx', $fnREC, "\n";
          system("/bin/rm -f $WorkDir/$fnREC");
          open(FR, ">$WorkDir/$fnREC") || die "can't open record file $fnREC\n";
          print FR $#fnHDF+1, "\n";

          # to solve the ocean tiles, we add other indicate
          # if tile number is neagtive means ocean tiles, otherwise land tiles
          foreach $fnLST (@fnHDF)
          {

             if($fnLST=~/^OCN/ || $fnLST=~/^DNO/)
             {
                $fnFIL = $fnLST;
                $fnFIL =~/h(\d+)v(\d+)/;
                print FR "-$1 -$2 $fnFIL \n";

             }
             else
             {
                # +xum change to sub reference
                # $fnFIL = &mask_data_veg($fnLST, $NsdsVEG, $MaskVEG);

                if($fnREC!~/43B/) {$fnFIL = $MaskFun->($fnLST, $NsdsMDS, $MaskMDS);}
                else              {$fnFIL = $fnLST;}
                $fnLST =~/h(\d+)v(\d+)/;
                print FR "+$1 +$2 $fnFIL \n";
             }
          }
          close(FR);
      }

      # command line subs
      #
      sub help_messages()
      {
          print "\n $0 is the perl script to handle the MODIS data for CWRF and is part of iCWPS \n\n";
          print << "ENDOFHELP";
          Usage : $0 [option[,option[,option ...]]]

          Options:
            -p PREFIX   --prefix=PREFIX      Put the SBCs data under PREFIX, such as PREFIX/iCWPS_1km, PREFIX/MODIS etc, default is upper directory. 
            -n NAMELIST --namelist=NAMELIST  Namelist file: NAMELIST.
            -v          --version            Display the version of this script.
            -h          --help               Display this help.
            -s          --SOM                Process the soil moisture data, 0 - monthly, 1 - 3hourly.
            -a          --ALB                Process the albedo data.
            -l          --LAI                Process the LAI/SAI.
            -u          --VEG                Process the landuse type.
            -i          --MVI                Process the vegetation index.
                        --IVT                Process AVHRR USGS24 and IGBP17
                        --FVC                Process fraction of vegetation
            -f          --fill               Fill the missing values.
ENDOFHELP
          print $Version;
          exit;
      }
